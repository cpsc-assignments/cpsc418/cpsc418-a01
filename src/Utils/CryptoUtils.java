/**
 * @author Artem Golovin
 * @id     30018900
 * @email  artem.golovin@ucalgary.ca
 *
 * @file   CryptoUtils.java
 */

package Utils;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.security.*;

public class CryptoUtils {
    public static byte[] SHA256Hash(byte[] inputData) throws NoSuchAlgorithmException {
        MessageDigest sha1 = MessageDigest.getInstance("SHA-256");
        return sha1.digest(inputData);
    }

    public static SecureRandom getSecureRandom() throws NoSuchAlgorithmException {
        return SecureRandom.getInstance("SHA1PRNG");
    }

    public static SecretKey getPRNGKey(String seed) throws NoSuchAlgorithmException {
        SecureRandom secureRandom = getSecureRandom();
        secureRandom.setSeed(seed.getBytes());

        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(128, secureRandom);
        return keyGenerator.generateKey();
    }

    public static Cipher getAESCipher(int mode, IvParameterSpec ivParameterSpec, String seed) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(mode, getPRNGKey(seed), ivParameterSpec);
        return cipher;
    }
}
